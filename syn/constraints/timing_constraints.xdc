create_clock -period 8.000 -name U_GTP/U_GTX_INST/I -waveform {0.000 4.000} [get_pins U_GTP/U_GTX_INST/gtxe2_i/TXOUTCLK]
create_clock -period 8.000 -name U_GTP/U_GTX_INST/rx_rec_clk_bufin -waveform {0.000 4.000} [get_pins U_GTP/U_GTX_INST/gtxe2_i/RXOUTCLK]
create_clock -period 50.000 -name clk_20m_vcxo_i -waveform {0.000 25.000} [get_ports clk_20m_vcxo_i]
create_clock -period 8.000 -name gtp_dedicated_clk_p_i -waveform {0.000 4.000} [get_ports gtp_dedicated_clk_p_i]
set_property ASYNC_REG true [get_cells {U_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/gen_straight.clk_i_d0_reg}]
set_property ASYNC_REG true [get_cells {U_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/gen_straight.clk_i_d3_reg}]
set_clock_groups -asynchronous -group [get_clocks U_GTP/U_GTX_INST/I] -group [get_clocks cmp_dmtd_clk_pll_n_4]
set_property ASYNC_REG true [get_cells {U_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/gen_straight.clk_i_d0_reg}]
set_property ASYNC_REG true [get_cells {U_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/gen_straight.clk_i_d3_reg}]
set_clock_groups -asynchronous -group [get_clocks U_GTP/U_GTX_INST/rx_rec_clk_bufin] -group [get_clocks cmp_dmtd_clk_pll_n_4]
set_false_path -from [get_clocks cmp_dmtd_clk_pll_n_4] -to [get_clocks I]
