#! /bin/bash

function remove_old_prj() {
	echo "Removing old existing project..."
	rm -f vivado*
	rm -f build_gw_version.tcl
	rm -rf .Xil
	rm -rf wrc_2p_a7
}

function generate_gw_version_tcl_script() {
	echo "proc src {file args} {
	set argv \$::argv
	set argc \$::argc
	set ::argv \$args
	set ::argc [llength \$args]
	set code [catch {uplevel [list source \$file]} return]
	set ::argv \$argv
	set ::argc \$argc
	return -code \$code $return
	}" > build_gw_version.tcl
	echo "set cdir [pwd]" >> build_gw_version.tcl
	echo "cd $(pwd)/../ip_cores/gen7s-cores/modules/gw_version" >> build_gw_version.tcl
	echo "src date2hdl.tcl ./xwb_GWversion.vhd.in ./xwb_GWversion.vhd" >> build_gw_version.tcl
	echo "cd \$cdir" >> build_gw_version.tcl
	chmod +x ./build_gw_version.tcl
}

function set_up_and_generate_files() {
	remove_old_prj
	generate_gw_version_tcl_script

	# Create the GW Version IP core
	tclsh ./build_gw_version.tcl
}

script_name=`basename $0`
expected_folder="syn"
folder=${PWD##*/}

if [ "$folder" != "$expected_folder" ]
then
	echo_err "This script must be run from $expected_folder folder"
	echo_err "Please cd to $expected_folder and re-run $script_name"
	exit
fi

prj=$1

case $prj in
  "wrc-2p-bare" )
	set_up_and_generate_files
  # Create the Vivado project and the Block design
  vivado -mode batch -source wrc-2p-prj.tcl
	# Lauch Vivado IDE
	vivado wrc_2p_a7/wrc_2p_a7.xpr
	;;
  "clean")
	remove_old_prj
	;;
  * )
	echo "You must specify the option: clean or wrc-2p-bare"
	;;
esac
